////////////////////////////////
// Templates

data "template_file" "sup_service" {
  template = file("${path.module}/../templates/hab-sup.service")

  vars = {
    flags      = "--auto-update  --listen-gossip 0.0.0.0:9638 --listen-http 0.0.0.0:9631 --event-stream-application=${var.event-stream-application} --event-stream-environment=${var.event-stream-environment} --event-stream-site=${var.aws_region} --event-stream-url=${var.automate_ip}:4222 --event-stream-token=${var.automate_token}"
  }
}

data "template_file" "install_hab" {
  template = file("${path.module}/../templates/install-hab.sh")

  vars = {
    opts = var.hab_install_opts
  }
}

data "template_file" "audit_toml_linux_qa" {
  template = file("${path.module}/../templates/audit_linux.toml")

  vars = {
    automate_url   = var.automate_url
    automate_token = var.automate_token
    automate_user  = var.automate_user
    automate_environment = "qa"
  }
}

data "template_file" "audit_toml_linux_prod" {
  template = file("${path.module}/../templates/audit_linux.toml")

  vars = {
    automate_url   = var.automate_url
    automate_token = var.automate_token
    automate_user  = var.automate_user
    automate_environment = "production"
  }
}

data "template_file" "audit_toml_linux" {
  template = file("${path.module}/../templates/audit_linux.toml")

  vars = {
    automate_url   = var.automate_url
    automate_token = var.automate_token
    automate_user  = var.automate_user
    automate_environment = "service"
  }
}


data "template_file" "terraform_tfvars" {
  template = file("${path.module}/../templates/terraform.tfvars")

  vars = {
    automate_url   = var.automate_url
    automate_token = var.automate_token
    automate_user  = var.automate_user
    automate_ip = var.automate_ip
  }
}

data "template_file" "config_toml_linux" {
  template = file("${path.module}/../templates/config_linux.toml")

  vars = {
    automate_url   = var.automate_url
    automate_token = var.automate_token
    automate_user  = var.automate_user
  }
}

