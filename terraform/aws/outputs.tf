

output "gitlab_runner_public_ip" {
  value = aws_instance.gitlab_runner.public_ip
}


output "qa_app_server_public_ip" {
  value = aws_instance.qa_app_server.public_ip
}

output "prod_app_server_public_ip" {
  value = aws_instance.prod_app_server.public_ip
}
